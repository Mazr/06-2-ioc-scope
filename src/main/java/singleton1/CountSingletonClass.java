package singleton1;

import org.springframework.stereotype.Component;

@Component
public class CountSingletonClass {
    static public int count = 0;

    public CountSingletonClass() {
        count++;
    }
}
