package com.twuc.webApp;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependent {
    static public int count = 0;

    public SingletonDependent() {
        count++;
    }
}
