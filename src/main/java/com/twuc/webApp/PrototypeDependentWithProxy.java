package com.twuc.webApp;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "prototype", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PrototypeDependentWithProxy {
    static public int count = 0 ;
    public PrototypeDependentWithProxy() {
        count++;
    }

    public String getName() {
        return name;
    }
    private String name = "maze";

}
