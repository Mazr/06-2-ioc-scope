package com.twuc.webApp;

import com.twuc.webApp.PrototypeDependentWithProxy;
import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxyBatchCall {
    private PrototypeDependentWithProxy proxy;

    public SingletonDependsOnPrototypeProxyBatchCall(PrototypeDependentWithProxy proxy) {
        this.proxy = proxy;
    }
   public void callToString_twice() {
        this.toString();
        this.toString();
    }
}
