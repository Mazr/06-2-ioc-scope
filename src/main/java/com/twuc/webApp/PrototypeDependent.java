package com.twuc.webApp;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class PrototypeDependent {
    static public int count = 0;

    public PrototypeDependent() {
        count++;
    }
}
