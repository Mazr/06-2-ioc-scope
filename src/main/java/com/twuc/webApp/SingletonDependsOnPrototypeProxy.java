package com.twuc.webApp;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxy {
    public PrototypeDependentWithProxy getPrototypeDependentWithProxy() {
        getName();
        return prototypeDependentWithProxy;
    }

    private PrototypeDependentWithProxy prototypeDependentWithProxy;

    public SingletonDependsOnPrototypeProxy(
            PrototypeDependentWithProxy prototypeDependentWithProxy) {
        this.prototypeDependentWithProxy = prototypeDependentWithProxy;
    }
    public void getName(){
        prototypeDependentWithProxy.getName();
    }



}
