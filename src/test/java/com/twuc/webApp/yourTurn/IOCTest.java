package com.twuc.webApp.yourTurn;

import com.twuc.webApp.*;
import com.twuc.webApp.PrototypeDependentWithProxy;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import singleton1.CountSingletonClass;
import org.junit.jupiter.api.Test;
import com.twuc.webApp.SingletonDependsOnPrototypeProxyBatchCall;


import static org.junit.jupiter.api.Assertions.*;

public class IOCTest {
    AnnotationConfigApplicationContext context = 
                new AnnotationConfigApplicationContext("com.twuc.webApp");

    @Test
    void should_return_the_same_object() {
        InterfaceOne bean = context.getBean(InterfaceOne.class);
        InterfaceOneImpl bean1 = context.getBean(InterfaceOneImpl.class);
        assertSame(bean,bean1);
    }

    @Test
    void should_throw_exception() {
        assertThrows(Exception.class,() -> {
            context.getBean(BaseClass.class);
        });
    }

    @Test
    void should_return_same_object_with_extends_abstract() {
        DerivedClass bean = context.getBean(DerivedClass.class);
        AbstractBaseClass bean1 = context.getBean(AbstractBaseClass.class);
        assertSame(bean,bean1);
    }

    @Test
    void should_get_different_object() {
        SimplePrototypeScopeClass bean = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass bean1 = context.getBean(SimplePrototypeScopeClass.class);
        assertNotSame(bean,bean1);

    }

    @Test
    void should_return_count_equals_2() {
        CountPrototypeScopeClass bean = context.getBean(CountPrototypeScopeClass.class);
        assertEquals(1,CountPrototypeScopeClass.count);
        CountPrototypeScopeClass bean1 = context.getBean(CountPrototypeScopeClass.class);
        assertEquals(2,CountPrototypeScopeClass.count);
    }
//TODO
    @Test
    void should_return_count_equals_1() {
        AnnotationConfigApplicationContext singletonContext =
                new AnnotationConfigApplicationContext("singleton1");

        CountSingletonClass bean = singletonContext.getBean(CountSingletonClass.class);
        assertEquals(1,CountSingletonClass.count);
        CountSingletonClass bean1 = singletonContext.getBean(CountSingletonClass.class);
        assertEquals(1,CountSingletonClass.count);

    }

    @Test
    void should_return_not_same_object_with_get_bean() {
        AnnotationConfigApplicationContext context1 = new AnnotationConfigApplicationContext("com.twuc.webApp");
        SingletonClass bean = context.getBean(SingletonClass.class);
        SingletonClass bean1 = context1.getBean(SingletonClass.class);
        assertNotSame(bean,bean1);
    }

    @Test
    void should_return_1() {
        PrototypeScopeDependsOnSingleton bean = context.getBean(PrototypeScopeDependsOnSingleton.class);
        PrototypeScopeDependsOnSingleton bean1 = context.getBean(PrototypeScopeDependsOnSingleton.class);
        assertEquals(1,SingletonDependent.count);
    }

    @Test
    void should_return_1_again() {
        SimplePrototypeScopeClass bean = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass bean1 = context.getBean(SimplePrototypeScopeClass.class);
        assertEquals(1,PrototypeDependent.count);
    }

    @Test
    void should_create_denpedent_class_repeatedly() {
        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
        bean.getName();
        assertEquals(1, PrototypeDependentWithProxy.count);
        bean.getName();
        assertEquals(2,PrototypeDependentWithProxy.count);
    }

    @Test
    void should_return_0() {
        SingletonDependsOnPrototypeProxyBatchCall bean =
                context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        PrototypeDependentWithProxy.count = 0;
        bean.callToString_twice();
        assertEquals(0, PrototypeDependentWithProxy.count);
    }

    @Test
    void should_create_the_same_proxy_object() {
        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
        assertSame(bean.getPrototypeDependentWithProxy(),bean.getPrototypeDependentWithProxy());
    }
}